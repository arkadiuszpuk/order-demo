<?php
namespace Domain;

use Domain\Couriers\Courier;

class Consignment
{
  private $id;
  private $courier;

  public funcition __construct(Courier $courier)
  {
    $this->courier = $courier; //we will need to recognize concrete courier type in sendService
    $this->id = $this->courier->generateConsignmentId(); //why private and in constructor - I need to be sure that will be immutable
  }

  public function getId()
  {
    return $this->id;
  }
}
