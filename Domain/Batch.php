<?php
namespace Domain;

use Domain\Couriers\Courier;
use Domain\Consignment;
use Domain\SendService\Client as SendService;

class Batch
{
  const STATUS_OPEN = 1;
  const STATUS_CLOSED = 2;

  private $courier;
  private $repository; //we want to have backup of all consignments in case of sendService will fail
  private $dispatchPeriod;
  private $status;
  private $consignments = [];

  public function __construct(Repository $repository, SendService $sendService)
  {
    $this->repository = $repository;
    $this->sendService = $sendService;
  }

  public function startNewBatch()
  {
    if ($this->status == self::STATUS_OPEN) {
      throw new \Exception('Cant start batch twice, please close it first'); //TODO add some specific Exception types
    }

    $this->dispatchPeriod = time(); //this line will be hard to test if I will have more time I will use some SystemFunction service locator for testing purpose
    $this->status = self::STATUS_OPEN;
  }

  public function addConsignment(Courier $courier)
  {
    if ($this->status != self::STATUS_OPEN) {
      throw new \Exception('Open Batch first');
    }

    $this->consignmentList[] = new Consignment($courier);
  }

  public function endCurrentBatch()
  {
    //all status validators in this class are duplicated - move to separate method or class
    if ($this->status != self::STATUS_OPEN) {
      throw new \Exception('There is no opened batch');
    }

    try {
      $this->repository->saveBatch($this->consignmentList);
      $this->sendService->send($this->consignmentList);
    } catch (\Exception $exception) {
      throw new \Exception('Cant end current batch'); //add specific exception type
    }

    $this->status = self::STATUS_CLOSED;
  }
}
