<?php
namespace Domain\Couriers;

interface Courier
{
  public function generateConsignmentId();
}
