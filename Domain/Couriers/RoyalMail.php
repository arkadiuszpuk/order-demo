<?php
namespace Domain\Couriers;

class RoyalMail implements Courier
{
  public function generateConsignmentId()
  {
    return uniqid('royalMail');
  }
}
