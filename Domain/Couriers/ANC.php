<?php
namespace Domain\Couriers;

class ANC implements Courier
{
  public function generateConsignmentId()
  {
    return uniqid('anc');
  }
}
